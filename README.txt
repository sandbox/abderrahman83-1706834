
-- SUMMARY --

Mythemes adds a block to allow users :
  - to enable and set a default theme.
  - to switch themes on the fly.
The module provides a list of all themes and allow users to choose between them.

For a full description visit the project page:
  http://drupal.org/project/metamor
Bug reports, feature suggestions and latest developments:
  http://drupal.org/project/issues/metamor
  
-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

* Enable the module in Administration � Modules.


-- CONFIGURATION --

* Configure who should be allowed to use the Mythemes blocks in
  Administration � People � Permissions � Mythemes.

* Customize the settings in Administration � Configuration � User interface �
  Mythemes.

  the settings page allows you to exclude one or more themes from the list,
  to hide or display message when a theme has been enabled or set default,
  to customize the submit button text,and finally to use or not the selected theme as default theme. 

* Go to Administration � Structure � Blocks and enable the Mythemes
  block. Mythemes block is automatically enabled in themes an added to the region sidebar_first.
