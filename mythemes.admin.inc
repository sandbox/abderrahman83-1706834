<?php

/**
 * @file
 * Administrative page callbacks for mythemes module.
 */
 
/*
 * Implements mythemes_settings_form()
 */  
 
function mythemes_settings_form($form, &$form_state){

  $keys = array_keys(list_themes(TRUE));
  $keys[] = t('none');
  $form['exclude'] = array(
   '#type' => 'fieldset', 
   '#title' => t('Exclude'), 
   '#collapsible' => TRUE, 
   '#collapsed' => FALSE,
  );
  $form['exclude']['theme_exclude'] = array(
    '#type' => 'select',
	'#title' => t('Themes excluded from the list'),
    '#multiple' => TRUE,
	'#size' => 10,
	'#options' => drupal_map_assoc($keys),
	'#default_value' => variable_get('theme_exclude',array(t('none'))),
  );
  $form['messages'] = array(
   '#type' => 'fieldset', 
   '#title' => t('Messages'), 
   '#collapsible' => TRUE, 
   '#collapsed' => TRUE,
  );
  $form['messages']['message_enabled'] = array(
    '#type' => 'checkbox',
	'#title' => t('Display a message when a theme has been enabled'),
    '#default_value' => variable_get('message_enabled',0),
  );
  $form['messages']['message_default'] = array(
    '#type' => 'checkbox',
	'#title' => t('Display a message when a theme has been set default'),
    '#default_value' => variable_get('message_default',0),
  );
  $form['more'] = array(
   '#type' => 'fieldset', 
   '#title' => t('More'), 
   '#collapsible' => TRUE, 
   '#collapsed' => TRUE,
  );
  $form['set_default_theme'] = array(
    '#type' => 'checkbox',
	'#title' => t('Set the selected theme as default theme.'),
    '#default_value' => variable_get('set_default_theme',0),
  );
  $form['more']['button_text'] = array(
    '#type' => 'textfield',
	'#title' => t('Submit button text'),
	'#description' => t('Enter replacement text for the submit button.'),
    '#default_value' => variable_get('button_text',t('Switch')),
  );
  return system_settings_form($form);
} 

